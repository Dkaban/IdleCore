# IdleCore

Building the core for an Idle game using RESTful and a CI process.

Technologies Used:
- Unity, C#
- https://designer.mocky.io/ : To test REST API Integration
- https://gitlab.com/ : To test CI Development
